﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp19
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] vect;
            vect = new int [4];
            Console.WriteLine("Introduceti 4 numere consecutive!");
            bool consec = true;
            for(int i=0;i<4;i++)
            {
                if (int.TryParse(Console.ReadLine(), out int nr))
                {
                    vect[i] = nr;
                }
                else
                {
                    Console.WriteLine("Numarul introdus nu are un format acceptat!");
                    i--;
                }
            }
            for(int i=0;i<vect.Length-1;i++)
            {
               if(Math.Abs(vect[i]-vect[i+1])!=1)
                {
                    consec = false;
                    Console.WriteLine("Numerele introduse nu sunt consecutive!!");
                    break;
                }

            }
            if(consec==true)
            {
                double rad;
                int produs=1;
                for(int i=0;i<vect.Length;i++)
                {
                    produs = produs * vect[i];
                    
                }
                rad = Math.Sqrt(produs);
                Console.WriteLine("Rezultatul este: "+rad);
            }
            Console.ReadKey();
        }
    }
}
